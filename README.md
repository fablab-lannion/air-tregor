# Air-tregor

le but du projet est de construire un capteur autonome de qualité de l'air raccordé au réseau LoRa TTN.
Plus de détails sur le wiki du fablab: https://wiki.fablab-lannion.org/index.php?title=AirTregor

## ESP32-Paxcounter
**Wifi & Bluetooth driven, LoRaWAN enabled, battery powered mini Paxcounter built on cheap ESP32 LoRa IoT boards**

Le code est un fork du code https://github.com/cyberman54/ESP32-Paxcounter
A l'origine le but était de compter le nombre de devices autour du TTGO.
Des capteurs ont été rajoutés et notamment le SDS011 (capteur de microparticules)

## Hardware

- TTGO: [T-Display](https://www.aliexpress.com/item/33048962331.html)
- TTGO: [T-Wristband](https://www.aliexpress.com/item/4000527495064.html)

Schéma du TTGO <A HREF="./img/TTGO.jpeg"></A>

Les ports utilisés:

- 4: écran MY_DISPLAY_SDA
- 5: LoRA SPI_CS
- 12: capteur, à connecter sur le RX du SDS011
- 15: écran MY_DISPLAY_SCL
- 16: écran MY_DISPLAY_RST
- 18: LoRA SPI_SCLK
- 19: LoRA SPI_MISO
- 23: LoRA SPI_MOSI
- 35: capteur, à connecter sur le TX du SDS011

Normalement y a rien à faire, le bon TTGO est sélectionné `platformio.ini`.<br>

### Boitiers

Des boîtiers imprimables en <b>3D</b> peuvent être trouvés sous Thingiverse: 

<A HREF="https://www.thingiverse.com/thing:2670713">Heltec</A>, 
<A HREF="https://www.thingiverse.com/thing:2811127">TTGOv2</A>, 
<A HREF="https://www.thingiverse.com/thing:3005574">TTGOv2.1</A>, 
<A HREF="https://www.thingiverse.com/thing:3385109">TTGO</A>,
<A HREF="https://www.thingiverse.com/thing:3041339">T-BEAM</A>, 
<A HREF="https://www.thingiverse.com/thing:3203177">T-BEAM parts</A>, 

<br>

## Installation de Platformio

Installer <A HREF="https://platformio.org/">PlatformIO IDE for embedded development</A> pour ce projet. Si vous utilisez Visual studio, il existe un plugins Platformio.

Les fichiers suivants doivent être adaptés:

- platformio.ini
- paxcounter.conf
- src/lmic_config.h
- src/loraconf.h

## Configuration de l'application
### platformio.ini

Editer `platformio_orig.ini`, par rapport au projet initial, on a conservé uniquement les cartes TTGO.

### paxcounter.conf

Editer `src/paxcounter.conf` pour personnaliser l'application paxcounter.
Mais a priori rien à faire...c'est déjà fait.

### src/lmic_config.h

Editer `src/lmic_config.h` pour la personnalisation de la configuraiton LoRa.
Normalement y a rien à faire car on a configuré le réseau européen pour LoRaWAN.

### src/loraconf.h

Editer le fichier `src/loraconf.h`
On doit préciser les paramètres LoRA des devices
On trouve ces informations sur la page https://eu1.cloud.thethings.network/console/applications/air-tregor (vous devez avoir un compte TTN et faire partie du groupe air-tregor)

Indiquer les bonnes valeurs de DEVEUI, APPEUI et APPKEY:

```bash
static const u1_t DEVEUI[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

static const u1_t APPEUI[8] = {0x70, 0xB3, 0xD5, 0x00, 0x00, 0x00, 0x00, 0x00};

static const u1_t APPKEY[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
```

[Doc TTN](https://www.thethingsnetwork.org/docs/devices/registration.html).

### src/ota.conf

Editer `src/ota.conf` sur la base de [src/ota.sample.conf](https://github.com/cyberman54/ESP32-Paxcounter/blob/master/src/ota.sample.conf), vous pouvez entrer les crédentials WIFI pour l'OTA.

## Compilation

Lancer la compile via le plugin <A HREF="https://platformio.org/">PlatformIO</A>.

## Aspects légaux

[Réglement européen s'appliquant donc à la Bretagne](https://edpb.europa.eu/news/national-news/2021/dutch-dpa-fines-municipality-wi-fi-tracking_en)

## LED blink pattern

**Mono color LED:**

- Single Flash (50ms): seen a new Wifi or BLE device
- Quick blink (20ms on each 1/5 second): joining LoRaWAN network in progress or pending
- Small blink (10ms on each 1/2 second): LoRaWAN data transmit in progress or pending
- Long blink (200ms on each 2 seconds): LoRaWAN stack error

**RGB LED:**

- Green: seen a new Wifi device
- Magenta: seen a new BLE device
- Yellow: joining LoRaWAN network in progress or pending
- Pink: LORAWAN MAC transmit in progress
- Blue: LoRaWAN data transmit in progress or pending
- Red: LoRaWAN stack error

## Display

Les pages affichées sur le TTGO sont: 

- Comptage PAX (nombre de devices WIFI/Bluetooth)
- Heure
- Valeurs des capteurs de particules


## Capteurs

Le code permet de définir 3 capteurs mais dans le cadre de notre projet on s'intéresse au capteur de particules.

## Integration dans TTN V3"

TODO

## Visualisation des capteurs

On utilise <A HREF="https://opensensemap.org/">openSenseMap</A>.
On déclare le device et on définit la <A HREF="./img/opensense-map.png">localisation</A>.
On peut voir les <A HREF="./img/opensense-pm10.png">valeurs du capteur</A> sur le site.

### Déclarer un nouveau device sur OpenSenseMap

TODO

# License

Copyright  2020-2021 Fablab Lannion

Copyright  2018-2020 Oliver Brandmueller <ob@sysadm.in>

Copyright  2018-2020 Klaus Wilting <verkehrsrot@arcor.de>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

NOTICE: 
Parts of the source files in this repository are made available under different licenses,
see file <A HREF="https://github.com/cyberman54/ESP32-Paxcounter/blob/master/LICENSE">LICENSE.txt</A> in this repository. Refer to each individual source file for more details.

# Credits

Thanks to 
- [Tangi Lavanant](https://gitlab.com/FabTangi) Fablab Lannion
- [Morgan Richomme](https://gitlab.com/colvert) Fablab Lannion
- [Oliver Brandmüller](https://github.com/spmrider) for idea and initial setup of this project
- [Charles Hallard](https://github.com/hallard) for major code contributions to this project
- [robbi5](https://github.com/robbi5) for the payload converter
- [Caspar Armster](https://www.dasdigidings.de/) for the The Things Stack V3 payload converter
- [terrillmoore](https://github.com/mcci-catena) for maintaining the LMIC for arduino LoRaWAN stack
- [sbamueller](https://github.com/sbamueller) for writing the tutorial in Make Magazine
- [Stefan](https://github.com/nerdyscout) for paxcounter opensensebox integration
- [August Quint](https://github.com/AugustQu) for adding SD card data logger and SDS011 support
