# Air Tregor

Une mesure de la qualité de l'air en Trégor

#HSLIDE
[drop=5 0]
## Air Trégor : Projet collaboratif

[drop=center]
![width=400, height=400](/img/logo_seul.png)

[drop=topleft]
![width=305, height=230](/img/TheThingsNetwork-logo-RGB.png)

[drop=bottomleft]
![width=200, height=200](/img/LeDantec.png)

[drop=bottomright]
![](/img/LuftDaten.png)

[drop=topright]
![](/img/Conseil22.png)
#HSLIDE

[drop=10 0]
## Air Trégor : Projet Innovant

[drop=20 20]
### Des solutions institutionnelles :
- [Air Paris](https://www.airparif.asso.fr)
- [Air Breizh](https://www.airbreizh.asso.fr/) 

[drop=10 50]
### Une solution citoyenne : [Sensors.community](https://sensor.community) 
- Coût maîtrisé
- Appropriation facilitée

- Implication de la communauté

#HSLIDE
[drop=10 0]
## Air Trégor : coût maîtrisé
[drop=20 25]
- Capteur : 15 €
[drop=55 25]
![width=200, height=200](/img/sds011-large.png)

[drop=20 50]
- Module LoRa : 15 €
[drop=55 50]
![width=200, height=200](/img/TTGO.jpeg)

[drop=20 70]
- Panneau Solaire: 15 €
[drop=55 70]
![width=200, height=200](/img/solar.jpeg)

[drop=20 90]
- Prix de vente : 50 €

#HSLIDE
[drop=25 0]
## Air Trégor : La suite 

[drop=30 35]
- Finaliser le prototype
- 2 Ateliers de mise en œuvre
- Appropriation de la donnée par des tiers
- Élargir les grandeurs mesurées
- Amélioration du réseau [TTN](https://www.thethingsnetwork.org/)

#HSLIDE
[drop=15 0]
## Air Tregor : Ober ar Vad 

[drop=20 30]
- Permettre d'adapter les activités à l'environnement

- Permettre une meilleure connaissance de l’environnement

- Permettre une appropriation technologique

- Participation à la [Politique Publique de la donnée](https://www.mission-open-data.fr/)
#HSLIDE
![Logo Fablab](http://static.fablab-lannion.org/logo_seul.png)
